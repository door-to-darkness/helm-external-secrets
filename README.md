# Helm - External Secrets
The [External Secrets Operator](https://github.com/external-secrets/external-secrets) reads information from a third party service like AWS Secrets Manager and automatically injects the values as Kubernetes Secrets.
